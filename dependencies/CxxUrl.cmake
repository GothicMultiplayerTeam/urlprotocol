add_library(CxxUrl STATIC)

target_sources(CxxUrl
    PUBLIC
        "CxxUrl/url.hpp"
        "CxxUrl/string.hpp"
    PRIVATE
        "CxxUrl/url.cpp"
)

target_include_directories(CxxUrl
    PUBLIC
		"CxxUrl/"
)