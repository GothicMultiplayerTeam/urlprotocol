#include <string>
#include <vector>
#include <cctype> // isspace

#include <Windows.h>

#include <url.hpp>

char sMessageBoxTitle[] = "G2O URL Protocol";

enum ExitCode
{
	OK,
	URL_ARGUMENT_REQUIRED,
	INVALID_URL,
	FAILED_TO_READ_GAME_DIR,
	FAILED_TO_RUN_GAME,
};

static void warning(const std::string& message)
{
	MessageBoxA(NULL, message.c_str(), sMessageBoxTitle, MB_ICONWARNING | MB_OK);
}

static void error(const std::string& message)
{
	MessageBoxA(NULL, message.c_str(), sMessageBoxTitle, MB_ICONERROR | MB_OK);
}

static const char* GetGothicExe()
{
	HKEY hKey;
	if (RegOpenKeyExA(HKEY_CURRENT_USER, "SOFTWARE\\G2O", 0, KEY_READ, &hKey) != ERROR_SUCCESS)
		return nullptr;

	static char gothic_exe_dir[MAX_PATH];
	DWORD gothic_exe_dir_size = sizeof(gothic_exe_dir);

	DWORD entry_type;

	if (RegQueryValueExA(hKey, "game_dir", nullptr, &entry_type, reinterpret_cast<LPBYTE>(gothic_exe_dir), &gothic_exe_dir_size) != ERROR_SUCCESS || entry_type != REG_SZ)
	{
		RegCloseKey(hKey);
		return nullptr;
	}
	
	char gothic_exe[] = "\\System\\Gothic2.exe";
	strcpy_s(gothic_exe_dir + strlen(gothic_exe_dir), sizeof(gothic_exe), gothic_exe);

	RegCloseKey(hKey);
	return gothic_exe_dir;
}

static std::string URLQueryToArgs(const std::string& address, const Url::Query& query)
{
	std::string args;
	args += " --connect " + address;

	for (auto& element : query)
	{
		const std::string& key = element.key();
		const std::string& value = element.val();

		// skip invalid param
		if (key.empty())
			continue;

		args += " --" + key + " " + value;
	}

	return args;
}

bool RunProgram(const std::string& executable, const std::string& command_line)
{
	STARTUPINFOA si = { sizeof(STARTUPINFOA) };
	PROCESS_INFORMATION pi = {};

	std::string working_dir = executable.substr(0, executable.rfind('\\'));
	std::string run_command = executable + command_line;

	if (!CreateProcessA(
		nullptr,                       // Application name (optional when using command line)
		&run_command[0],               // Command line (must be writable)
		nullptr,                       // Process security attributes
		nullptr,                       // Thread security attributes
		FALSE,                         // Inherit handles
		0,                             // Creation flags
		nullptr,                       // Environment (nullptr means inherit)
		&working_dir[0],               // Current directory
		&si,                           // Startup info
		&pi                            // Process information
	))
		return false;

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return true;
}

static std::vector<std::string> CommandLineToArguments(LPSTR cmd_line)
{
	std::vector<std::string> arguments;

	std::string arg;
	bool inQuote = false;

	for (unsigned int i = 0, end = strlen(cmd_line); i < end; ++i)
	{
		char c = cmd_line[i];

		if (c != '"' && (inQuote || !isspace(c)))
			arg += c;
		else
		{
			if (!arg.empty())
				arguments.push_back(arg);

			arg = "";
		}

		if (c == '"')
		{
			inQuote = !inQuote;

			if (!inQuote)
			{
				if (!arg.empty())
					arguments.push_back(arg);

				arg = "";
			}
		}
	}

	return arguments;
}

int main(int argc, char* argv[])
{
	if (argc <= 1)
	{
		error("URL argument required!");
		return URL_ARGUMENT_REQUIRED;
	}

	Url url(argv[1]);

	std::string address;
	Url::Query url_query;

	try
	{
		address = url.host() + ":" + url.port();
		url_query = url.query();

		if (url.scheme() != "g2o")
			return INVALID_URL;
	}
	catch (Url::parse_error ex)
	{
		error(std::string("Invalid URL!\n") + ex.what());
		return INVALID_URL;
	}

	const char* gothic_exe = GetGothicExe();
	if (gothic_exe == nullptr)
	{
		error("Failed to read game_dir!");
		return FAILED_TO_READ_GAME_DIR;
	}

	if (!RunProgram(gothic_exe, URLQueryToArgs(address, url_query)))
	{
		error("Failed to run Gothic2.exe!");
		return FAILED_TO_RUN_GAME;
	}

	return OK;
}

int APIENTRY WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
	std::vector<std::string> arguments = CommandLineToArguments(GetCommandLineA());
	std::vector<char*> argv;

	for (auto& arg : arguments)
		argv.push_back(&arg[0]);

	return main(argv.size(), &argv[0]);
}
