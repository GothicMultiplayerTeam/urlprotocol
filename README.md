# Introduction

This project adds the `g2o://` protocol feature for [**Gothic 2 Online**](https://gothic-online.com/).  
The URL Protocol allows you to join into the server more conveniently (by just pressing on a link).

# Protocol registration

There are two ways to register a `g2o://` protocol, you can either:
- Install [**Gothic 2 Online**](https://gothic-online.com/) modification.
- Use **register-protocol.reg** script to register it.  
	**NOTE!** make sure to edit the path inside this file, to match your **G2O_URLProtocol.exe** location!  
	
You can also unregister a protocol by double clicking on **unregister-protocol.reg**.

# Usage

#### Joining up to local server
- [g2o://127.0.0.1:28970](g2o://127.0.0.1:28970)

#### Joining up to local server with custom nickname
- [g2o://127.0.0.1:28970?nickname=Patrix](g2o://127.0.0.1:28970?nickname=Patrix)

#### Joining up to local server with custom parameter
- [g2o://127.0.0.1:28970?token=blablabla](g2o://127.0.0.1:28970?token=blablabla)

#### Joining up to local server with custom parameters
- [g2o://127.0.0.1:28970?token=blablabla&account_id=1337](g2o://127.0.0.1:28970?token=blablabla&account_id=1337)